import pandas as pd
import numpy as np
import glob
import os
import plotly.plotly as py
from plotly.graph_objs import *
get_ipython().run_line_magic('matplotlib', 'inline')


#read the data from airbnb
df = pd.read_csv('grenoble_parsed.txt')

#read the seperate csvs, and combine into dataframe
df1 = pd.read_csv('vdata1.csv', sep=';')
df1.dropna(axis=0)
#modify path as necessary
path =r'/home/spencer/urbanplanning/data-mining-for-urban-planning-vasily-084da10c7fae45c298f4b31ba7153a1a638e9841/data/grenoble' # use your path
allFiles = glob.glob(os.path.join(path, "*.csv"))
df_from_each_file = (pd.read_csv(f, sep=';') for f in allFiles)
df1   = pd.concat(df_from_each_file, ignore_index=True)
df1.dropna(axis = 0)

df_remove_outliers = df[np.abs(df.price-df.price.mean())<=(3*df.price.std())]
df_remove_outliers


# In[3]:

# parse the unnecessary stuff from leboncoin
df1['loyer'] = df1['loyer'].map(lambda x: x.rstrip(','))
df1['pieces'] = df1['pieces'].astype(str).map(lambda x: x.rstrip(','))
df1['surface'] = df1['surface'].map(lambda x: x.rstrip(','))
df1[['loyer']] = df1[['loyer']].apply(pd.to_numeric)
df1[['pieces']] = df1[['pieces']].apply(pd.to_numeric, errors='coerce')
df1[['surface']] = df1[['surface']].apply(pd.to_numeric)


# In[19]:

# normalize the data by removing outliers
df1_remove_outliers = df1[np.abs(df1.loyer-df1.loyer.mean())<=(3*df1.loyer.std())]
df1_remove_outliers
df1.dropna(axis = 0)
df1 = df1.drop('type', 1)


# In[41]:


df1 = df1.dropna(axis = 0)


# In[42]:


df1.to_csv("cleaned_data.csv")
df1


# In[119]:
#histogram

df.hist(column='price', bins=30)


# In[120]:

#histogram
df1.hist(column='loyer', bins=30)


# In[146]:

#labels for hover data
df1['text'] = 'Price:' + df1['loyer'].astype(str) + ', Area: ' + df1['surface'].astype(str) + ', Rooms: ' + df1['pieces'].astype(str)
df['text'] = 'Price:' + df['price'].astype(str) + ', Capacity: ' + df['person_capacity'].astype(str) + ', Room Type: ' + df['room_type'].astype(str)

#the different datasets 
data = Data([
    Scattermapbox(
        lat=df1['latitude'],
        lon=df1['longitude'],
        mode='markers',
        marker=Marker(
            size=8,
            color =df1['loyer'],
            colorscale= 'Rainbow',
            cmin = min(df1['loyer'].astype(int)),
            cmax = max(df1['loyer'].astype(int)),
            showscale=True,
            symbol = 'circle',
            colorbar = dict(
                thickness = 10,
                titleside = "right",
                outlinecolor = "rgba(68, 68, 68, 0)",
                ticks = "outside",
                ticksuffix = " Euros",
            ),
        ),
        text = df1['text'],

    ),
        Scattermapbox(
        lat=df1['latitude'],
        lon=df1['longitude'],
        mode='markers',
        marker=Marker(
            size=8,
            color =df1['surface'],
            colorscale= 'Rainbow',
            cmin = min(df1['surface'].astype(int)),
            cmax = max(df1['surface'].astype(int)),
            showscale=True,
            symbol = 'circle',
            colorbar = dict(
                thickness = 10,
                titleside = "right",
                outlinecolor = "rgba(68, 68, 68, 0)",
                ticks = "outside",
                ticksuffix = " M^2",
            ),
        ),
        text = df1['text'],

    ),
            Scattermapbox(
        lat=df1['latitude'],
        lon=df1['longitude'],
        mode='markers',
        marker=Marker(
            size=8,
            color =df1['pieces'].astype(float),
            colorscale= 'Rainbow',
            cmin = min(df1['pieces'].astype(float)),
            cmax = max(df1['pieces'].astype(float)),
            showscale=True,
            symbol = 'circle',
            colorbar = dict(
                thickness = 10,
                titleside = "right",
                outlinecolor = "rgba(68, 68, 68, 0)",
                ticks = "outside",
                ticksuffix = " Rooms",
            ),
        ),
        text = df1['text'],

    ),
        Scattermapbox(
        lat=df['lat'],
        lon=df['lng'],
        mode='markers',
        marker=Marker(
            size=8,
            color =df['price'],
            colorscale= 'Rainbow',
            cmin = min(df['price'].astype(int)),
            cmax = max(df['price'].astype(int)),
            showscale=True,
            symbol = 'circle',
            colorbar = dict(
                thickness = 10,
                titleside = "right",
                outlinecolor = "rgba(68, 68, 68, 0)",
                ticks = "outside",
                ticksuffix = " Euros",
            ),
        ),
        text = df['text'],

    ),
        Scattermapbox(
        lat=df['lat'],
        lon=df['lng'],
        mode='markers',
        marker=Marker(
            size=8,
            color =df['person_capacity'],
            colorscale= 'Rainbow',
            cmin = min(df['person_capacity'].astype(int)),
            cmax = max(df['person_capacity'].astype(int)),
            showscale=True,
            symbol = 'circle',
            colorbar = dict(
                thickness = 10,
                titleside = "right",
                outlinecolor = "rgba(68, 68, 68, 0)",
                ticks = "outside",
                ticksuffix = " People",
            ),
        ),
        text = df['text'],

    ),
        Scattermapbox(
        lat=df1_remove_outliers['latitude'],
        lon=df1_remove_outliers['longitude'],
        mode='markers',
        marker=Marker(
            size=8,
            color =df1_remove_outliers['loyer'],
            colorscale= 'Rainbow',
            cmin = min(df1_remove_outliers['loyer'].astype(int)),
            cmax = max(df1_remove_outliers['loyer'].astype(int)),
            showscale=True,
            symbol = 'circle',
            colorbar = dict(
                thickness = 10,
                titleside = "right",
                outlinecolor = "rgba(68, 68, 68, 0)",
                ticks = "outside",
                ticksuffix = " Euros",
            ),
        ),
        text = df1['text'],

    ),
        Scattermapbox(
        lat=df_remove_outliers['lat'],
        lon=df_remove_outliers['lng'],
        mode='markers',
        marker=Marker(
            size=8,
            color =df_remove_outliers['price'],
            colorscale= 'Rainbow',
            cmin = min(df_remove_outliers['price'].astype(int)),
            cmax = max(df_remove_outliers['price'].astype(int)),
            showscale=True,
            symbol = 'circle',
            colorbar = dict(
                thickness = 10,
                titleside = "right",
                outlinecolor = "rgba(68, 68, 68, 0)",
                ticks = "outside",
                ticksuffix = " Euros",
            ),
        ),
        text = df['text'],

    )
])

# 
updatemenus = list([
    dict(type="buttons",
         active=-1,
         buttons=list([   
            dict(label = 'Monthly Price',
                 method = 'update',
                 args = [{'visible': [True, False, False, False, False, False, False]},
                         {'title': 'Monthly Price'}]),
            dict(label = 'Monthly Price Adjusted',
                 method = 'update',
                 args = [{'visible': [False, False, False, False, False, True, False]},
                         {'title': 'Monthly Price Adjusted'}]),
            dict(label = 'Monthly Surface',
                 method = 'update',
                 args = [{'visible': [False, True, False, False, False, False, False]},
                         {'title': 'Monthly Surface'}]),
            dict(label = 'Monthly Pieces',
                 method = 'update',
                 args = [{'visible': [False, False, True, False, False, False, False]},
                         {'title': 'Monthly Surface'}]),
            dict(label = 'Day Price',
                 method = 'update',
                 args = [{'visible': [False, False, False, True, False, False, False]},
                         {'title': 'Daily Price'}]),
            dict(label = 'Day Price Adjusted',
                 method = 'update',
                 args = [{'visible': [False, False, False, False, False, False, True]},
                         {'title': 'Day Price Adjusted'}]),
            dict(label = 'Day Capacity',
                 method = 'update',
                 args = [{'visible': [False, False, False, False, True, False, False]},
                         {'title': 'Daily Capacity'}])
        ]),
    )
])
layout = Layout(
    title = 'Monthly, Daily, and Produced Data',
    autosize=True,
    hovermode='closest',
    width=900,
    height=700,
    mapbox=dict(
        accesstoken=mapbox_access_token,
        bearing=0,#
        center=dict(
            lat=45.18,
            lon=5.72
        ),
        pitch=0,
        zoom=11,
        style='light',


    ),
    updatemenus=updatemenus
)

fig = dict(data=data, layout=layout)
py.iplot(fig, filename='Multiple Mapbox')


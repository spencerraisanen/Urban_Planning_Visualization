This was the visualization aspect of a project wherein the results of mined
data was presented on maps using the Plotly library.

Output visible at https://plot.ly/~raisspen/2